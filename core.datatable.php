<?php

/*---------------------------------------------------------------------------------------------*/

class TDataTable extends TControl
{
	public  $aOptions = array();
	private $aCols    = array();
	
	/**/

	public function __construct( $oWnd, $cId = '', $nTop = 0, $nLeft = 0, $nWidth = null, $nHeight = null, $aData = array() )
	{
		$nWidth  = TDefault( $nWidth, TWEB_GRID_WIDTH_DEFAULT );
		$nHeight = TDefault( $nHeight, TWEB_GRID_HEIGHT_DEFAULT );

		parent::__construct( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeight );

		$this->cClass   = 'tweb_datatable';
		$this->cControl = 'tdatatable';
		$this->aData    = $aData;

		$this->SetBorderInset();

		$this->aOptions = array( 'info' => true,
								 'paging' => true,
								 'ordering' => true,
								 'searching' => true,
								 'scrollCollapse' => false,
								 'scrollX' => true,
								 'scrollY' => true,
								 'pageLength' => 25,
								 'dummy' => null
								);
	}

	/**/

	public function Info( $lOnOff = true ) { $this->aOptions[ 'info' ] = $lOnOff; }
	public function Paging( $lOnOff = true ) { $this->aOptions[ 'paging' ] = $lOnOff; }
	public function Ordering( $lOnOff = true ) { $this->aOptions[ 'ordering' ] = $lOnOff; }
	public function Searching( $lOnOff = true ) { $this->aOptions[ 'searching' ] = $lOnOff; }
	public function ScrollCollapse( $lOnOff = true ) { $this->aOptions[ 'scrollCollapse' ] = $lOnOff; }
	public function ScrollX( $lValue = true ) { $this->aOptions[ 'scrollX' ] = $lValue; }
	public function ScrollY( $uValue = true ) { $this->aOptions[ 'scrollY' ] = $uValue; }
	public function PageLength( $nValue = 25 ) { $this->aOptions[ 'pageLength' ] = $nValue; }

	/**/

	public function AddCol( $cId, $cHeader, $cFooter = null )
	{
		$oCol = new TDataTableCol( $cId, $cHeader, $cFooter );
		$this->aCols[] = $oCol;
		return $oCol;
	}

	/**/

	public function Activate()
	{
		$this->CoorsUpdate();

		$cHtml  = '<div id="div-' . $this->cId . '" ';

		$cHtml .= $this->DefClass();
		$cHtml .= $this->Datas();

		$cHtml .= 'style="position: absolute; ';
		$cHtml .= 'box-sizing: border-box; overflow: hidden; ';

		$cHtml .= $this->StyleDim();
		$cHtml .= $this->StyleColor();
		$cHtml .= $this->StyleCss();
		$cHtml .= $this->StyleShadow();

		$cHtml .= '">';

		$cHtml .= '<table id="' . $this->cId . '" class="display" cellspacing="0" width="100%"> ';

		$nCols = count( $this->aCols );

		$cHtml .= '<thead>';
		for ( $i = 0; $i < $nCols - 1; $i++ )
		{
			$cHeader = $this->aCols[ $i ]->cHeader;
			$cHtml .= '<th>' . $cHeader . '</th>';
		}

		$cHtml .= '</thead>';
		$cHtml .= '</table>';

		$cHtml .= '</div>' . PHP_EOL;

		$cHtml .= '<style>' . PHP_EOL;
		
		$cHtml .= '.dataTables_wrapper {' 								. PHP_EOL;
		$cHtml .= '	clear: both;' 												. PHP_EOL;
		$cHtml .= '	width: auto;' 												. PHP_EOL;
		$cHtml .= '	height: ' . ($this->nHeight-12) . ';' . PHP_EOL;
		$cHtml .= '	border-bottom: 1px solid black;' 			. PHP_EOL;
		$cHtml .= '	border-top: 1px solid black;' 				. PHP_EOL;
		$cHtml .= '	border-left: 1px solid black;' 				. PHP_EOL;
		$cHtml .= '	border-right: 1px solid black;' 			. PHP_EOL;
		$cHtml .= '	background-color: #dff9fa;' 					. PHP_EOL;
		$cHtml .= '	zoom: 1;' 														. PHP_EOL;
		$cHtml .= '	padding-top: 4px;' 										. PHP_EOL;
		$cHtml .= '	padding-bottom: 4px;' 								. PHP_EOL;
		$cHtml .= '}'                                 		. PHP_EOL;

		$cHtml .= '.dataTables_wrapper .dataTables_filter input {' 	. PHP_EOL;
		$cHtml .= '	margin-left: 0.5em !important;' 								. PHP_EOL;
		$cHtml .= '	margin-right: 3px !important;' 									. PHP_EOL;
		$cHtml .= '}' 																							. PHP_EOL;

		$cHtml .= '</style>' . PHP_EOL;

		echo $cHtml;
		
		$uInfo           = $this->aOptions[ 'info'           ] ? 'true' : 'false';
		$uPaging         = $this->aOptions[ 'paging'         ] ? 'true' : 'false';
		$uOrdering       = $this->aOptions[ 'ordering'       ] ? 'true' : 'false';
		$uSearching      = $this->aOptions[ 'searching'      ] ? 'true' : 'false';
		$uScrollCollapse = $this->aOptions[ 'scrollCollapse' ] ? 'true' : 'false';
		$uPageLength     = $this->aOptions[ 'pageLength'     ];
		$uScrollX        = $this->aOptions[ 'scrollX'        ] ? 'true' : 'false';
		if( gettype( $this->aOptions[ 'scrollY' ] ) == 'boolean' ) {
			$uScrollY    = $this->aOptions[ 'scrollY'        ] ? 'true' : 'false';
		} else {
			$uScrollY    = '"'. $this->aOptions[ 'scrollY' ] . '"';
		}
		
		$cFunction  = 'var dt_options = {';
		$cFunction .= ' "info" : ' . $uInfo . ',';
		$cFunction .= ' "paging" : ' . $uPaging . ',';
		$cFunction .= ' "ordering" : ' . $uOrdering . ',';
		$cFunction .= ' "searching" : ' . $uSearching . ',';
		$cFunction .= ' "scrollCollapse" : ' . $uScrollCollapse . ',';
		$cFunction .= ' "pageLength" : ' . $uPageLength . ',';
		$cFunction .= ' "scrollX" : ' . $uScrollX . ',';
		$cFunction .= ' "scrollY" : ' . $uScrollY;
		$cFunction .= '};';

		$cFunction .= 'var oGrid = new TDataTable2( "' . $this->cId . '", dt_options );';
		$cFunction .= 'var o = new TControl();';

		if ( gettype( $this->aData ) == 'array' && count( $this->aData ) > 0 )
		{
			$cFunction .= " oGrid.SetData( " . json_encode( $this->aData ) . " ); ";
		}
		
		$cFunction .= " o.ControlInit( '" . $this->cId . "' , oGrid, 'datatable' );";
		$cFunction .= "$( '#" . $this->cId . "' ).resize(); ";
		
		ExeJSReady( $cFunction );
	}

} /*end class TDataTable*/

/*---------------------------------------------------------------------------------------------*/

Class TDataTableCol
{
	public $cId     = '';
	public $cHeader = '';
	public $cFooter = '';

	/**/

	public function __construct( $cId, $cHeader, $cFooter )
	{
		$this->cId   = $cId;
		$this->cHeader = $cHeader;
		$this->cFooter = $cFooter;
	}

} /*End class TDataTableCol*/

/*---------------------------------------------------------------------------------------------*/

function console_php( $data )
{
	ob_start();
	$output  = "<script>console.log( 'PHP debugger: ";
	$output .= json_encode( print_r( $data, true ) );
	$output .= "' );</script>";
	echo $output;
}

/*---------------------------------------------------------------------------------------------*/
//EOF
/*---------------------------------------------------------------------------------------------*/
