<?php

/*------------------------------------------------------------------------------------------------*/

include( 'config.php' );
include( TWEB_PATH . 'core.php' );
include( 'core.datatable.php' );

/*------------------------------------------------------------------------------------------------*/

$oWeb = new TWeb( 'Prueba de TWeb con DataTable - 1.0a' );
$oWeb->lAwesome = true; 
$oWeb->lPreloader = true;

$oWeb->SetIcon( 'images/myicon.png' );
$oWeb->SetBrush( 'images/myback1.png' );

$oWeb->AddCss( 'mystyles.css' );
$oWeb->AddCss( 'libs/datatable/dataTables.min.css' );

$oWeb->AddJs( 'myfunctions.js' );
$oWeb->AddJs( 'libs/datatable/dataTables.min.js' );

$oWeb->AddCss( 'styles.css' );

$oWeb->Activate();
$oWeb->End();

?>
<style>



</style>

<script type="text/javascript">

var dataSet = [
    [ "Tiger Nixon", "System Architect", "Edinburgh", "5421", "2011/04/25", "$320,800" ],
    [ "Garrett Winters", "Accountant", "Tokyo", "8422", "2011/07/25", "$170,750" ],
    [ "Ashton Cox", "Junior Technical Author", "San Francisco", "1562", "2009/01/12", "$86,000" ],
    [ "Cedric Kelly", "Senior Javascript Developer", "Edinburgh", "6224", "2012/03/29", "$433,060" ],
    [ "Airi Satou", "Accountant", "Tokyo", "5407", "2008/11/28", "$162,700" ],
    [ "Brielle Williamson", "Integration Specialist", "New York", "4804", "2012/12/02", "$372,000" ],
    [ "Herrod Chandler", "Sales Assistant", "San Francisco", "9608", "2012/08/06", "$137,500" ],
    [ "Rhona Davidson", "Integration Specialist", "Tokyo", "6200", "2010/10/14", "$327,900" ],
    [ "Colleen Hurst", "Javascript Developer", "San Francisco", "2360", "2009/09/15", "$205,500" ],
    [ "Sonya Frost", "Software Engineer", "Edinburgh", "1667", "2008/12/13", "$103,600" ],
    [ "Jena Gaines", "Office Manager", "London", "3814", "2008/12/19", "$90,560" ],
    [ "Quinn Flynn", "Support Lead", "Edinburgh", "9497", "2013/03/03", "$342,000" ],
    [ "Charde Marshall", "Regional Director", "San Francisco", "6741", "2008/10/16", "$470,600" ],
    [ "Haley Kennedy", "Senior Marketing Designer", "London", "3597", "2012/12/18", "$313,500" ],
    [ "Tatyana Fitzpatrick", "Regional Director", "London", "1965", "2010/03/17", "$385,750" ],
    [ "Michael Silva", "Marketing Designer", "London", "1581", "2012/11/27", "$198,500" ],
    [ "Paul Byrd", "Chief Financial Officer (CFO)", "New York", "3059", "2010/06/09", "$725,000" ],
    [ "Gloria Little", "Systems Administrator", "New York", "1721", "2009/04/10", "$237,500" ],
    [ "Bradley Greer", "Software Engineer", "London", "2558", "2012/10/13", "$132,000" ],
    [ "Dai Rios", "Personnel Lead", "Edinburgh", "2290", "2012/09/26", "$217,500" ],
    [ "Jenette Caldwell", "Development Lead", "New York", "1937", "2011/09/03", "$345,000" ],
    [ "Yuri Berry", "Chief Marketing Officer (CMO)", "New York", "6154", "2009/06/25", "$675,000" ],
    [ "Caesar Vance", "Pre-Sales Support", "New York", "8330", "2011/12/12", "$106,450" ],
    [ "Doris Wilder", "Sales Assistant", "Sydney", "3023", "2010/09/20", "$85,600" ],
    [ "Angelica Ramos", "Chief Executive Officer (CEO)", "London", "5797", "2009/10/09", "$1,200,000" ],
    [ "Gavin Joyce", "Developer", "Edinburgh", "8822", "2010/12/22", "$92,575" ],
    [ "Jennifer Chang", "Regional Director", "Singapore", "9239", "2010/11/14", "$357,650" ],
    [ "Brenden Wagner", "Software Engineer", "San Francisco", "1314", "2011/06/07", "$206,850" ],
    [ "Fiona Green", "Chief Operating Officer (COO)", "San Francisco", "2947", "2010/03/11", "$850,000" ],
    [ "Shou Itou", "Regional Marketing", "Tokyo", "8899", "2011/08/14", "$163,000" ],
    [ "Michelle House", "Integration Specialist", "Sydney", "2769", "2011/06/02", "$95,400" ],
    [ "Suki Burks", "Developer", "London", "6832", "2009/10/22", "$114,500" ],
    [ "Prescott Bartlett", "Technical Author", "London", "3606", "2011/05/07", "$145,000" ],
    [ "Gavin Cortez", "Team Leader", "San Francisco", "2860", "2008/10/26", "$235,500" ],
    [ "Martena Mccray", "Post-Sales support", "Edinburgh", "8240", "2011/03/09", "$324,050" ],
    [ "Unity Butler", "Marketing Designer", "San Francisco", "5384", "2009/12/09", "$85,675" ]
];

</script>

<script src="core.datatable.js"></script>';

<?php

$oWnd = new TWindow( 'main', 10, '10%', '80%', '80%', 'white' );
  $oBar = new TBar( $oWnd );
	$oBar->AddButton( 'bAdd'    , 'Nuevo' , 'AddRow()' , 'images/bms_tadd.png'   , 'Agregar historial.' );
	$oBar->AddButton( 'bEdit'   , 'Edita' , 'EditRow()', 'images/bms_tedit.png'  , 'Editar historial.'  );
	$oBar->AddButton( 'bDel'    , 'Borra' , 'DelRow()' , 'images/bms_tdelete.png', 'Elimina historial.' );
	$oBar->Separator();
	$oBar->AddButton( 'bTest'   , 'Prueba', 'MyTest()' , 'images/bms_test.bmp'   , 'Código de prueba.' );	
	$oBar->Separator();
	$oBar->AddButton( 'bRefresh', 'Carga', 'Refresh()', 'images/bms_treload.png', 'Actualiza lista.' );

$oDataTable = new TDataTable( $oWnd, 'mydatatable', 60, 10, '97%', 500 );
$oCol = $oDataTable->AddCol( 'name'    , 'Nombre'        );
$oCol = $oDataTable->AddCol( 'position', 'Puesto'        );
$oCol = $oDataTable->AddCol( 'Office'  , 'Oficina'       );
$oCol = $oDataTable->AddCol( 'ext'     , 'Extencion'     );
$oCol = $oDataTable->AddCol( 'doh'     , 'Fecha inicial', 'Total C$' );
$oCol = $oDataTable->AddCol( 'salary'  , 'Salario'       );
$oDataTable->PageLength( 10 );
$oDataTable->Paging( true );
$oDataTable->ScrollY( "340px" );
$oDataTable->lVScroll = "260px";

$oWnd->Activate();

?>

<script type="text/javascript">

$(document).ready( function() 
{

});

function AddRow() {
	var oCtrl = new TControl();
	var oGrid = oCtrl.GetControl( 'mydatatable' );

	var aRow = [ "Carlos Vargas", "Programmer", "Managua", "44", "1973/11/02", "$700,800" ];

	if( oGrid.InsertRow( aRow ) ) {
		MsgNotify( 'Se agrego datos existosamente!', 'success', true );
	}

}

function EditRow() {
	var oCtrl = new TControl();
	var oGrid = oCtrl.GetControl( 'mydatatable' );

	var aRow = oGrid.EditSelectRow();
	if ( isArray( aRow ) ) {
		console.log( aRow );
		MsgNotify( 'Fila de datos recuperada exitosamente, vea la consola!', 'success', true );
	}
	else
	{
		MsgNotify( 'No ha seleccionado una fila o tabla vacia!', 'error', true );
	}
}

function DelRow() {
	var oCtrl = new TControl();
	var oGrid = oCtrl.GetControl( 'mydatatable' );

	if ( oGrid.DeleteSelectRow() )
	{
		MsgNotify( 'Fila eliminada!', 'success', true );
	}
	else
	{
		MsgNotify( 'No ha seleccionado una fila o tabla vacia!', 'error', true );
	}
}

function Refresh() {
	var oCtrl = new TControl();
	var oGrid = oCtrl.GetControl( 'mydatatable' );

	oGrid.SetData( dataSet );
	oGrid.SetFocus();

	MsgNotify( 'Numero de filas: ' + oGrid.Len(), 'info', true );
/*
	var table = $('#mydatatable').DataTable();
	table.cell( ':eq(0)' ).focus();
	table.row(':eq(0)', { page: 'current' }).select();
*/
}

function MyTest() {
	var oCtrl = new TControl();
	var oGrid = oCtrl.GetControl( 'mydatatable' );

	var cName = oGrid.GetColValue( 0 );
	MsgNotify('Prueba: ' +cName, 'info', true );

	console.log( oGrid.GetData() );
}

</script>

