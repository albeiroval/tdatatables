var TDataTable2 = function( cId, aOptions )
{
	this.cId      = cId;
	this.aOptions = aOptions;
	this.oGrid    = null;
	this.bSelect  = '';

	var oGrid = null;
	var oRow = null;
	var nRecno = 0;

	/**/

	this.Init = function() {
		oGrid = $( '#' + this.cId ).DataTable( {
			"info"           : this.aOptions[ 'info' ],
			"paging"         : this.aOptions[ 'paging' ],
			"ordering"       : this.aOptions[ 'ordering' ],
			"searching"      : this.aOptions[ 'searching' ],
			"scrollCollapse" : this.aOptions[ 'scrollCollapse' ],
			"pageLength"     : this.aOptions[ 'pageLength' ],
			"processing"     : true,
			"responsive"     : true,
			"scrollX"        : this.aOptions[ 'scrollX' ],
			"scrollY"        : this.aOptions[ 'scrollY' ],
			"keys"           : true,
			"sDom"           : "ftipr",
			"select"         : "single",
			"language" : {
				"url": "libs/datatable/spanish.json",
				"select" : {
					"rows" : ""
				}
			}
		} );

		oGrid.on( 'select', function (e, datatable, type, indexes ) {
			oRow = datatable.row( { selected: true } ).data();
		});

		// Handle click in data
		oGrid.on( 'click', 'tr', function () {
			nRecno = oGrid.row( this ).index();
			console.log( 'registro #: ' + nRecno );
		} );

		// Handle event when cell gains focus
		oGrid.on( 'key-focus.dt', function(e, datatable, cell) {
			$( datatable.row( cell.index().row ).node() ).addClass( 'selected' );

			nRecno = oGrid.row( cell.index().row ).index();
			console.log( 'registro #: ' + nRecno );
		});

		// Handle event when cell looses focus
		oGrid.on( 'key-blur.dt', function(e, datatable, cell) {
			$( datatable.row( cell.index().row ).node() ).removeClass( 'selected' );
		});

		// Handle keys events
		oGrid.on( 'key.dt', function(e, datatable, key, cell, originalEvent){
			console.log( 'Key: ' + key );
			switch ( key ) {
				case 13: //enter
					var data = datatable.row( cell.index().row ).data();
					console.log( data.join(', ') );
					break;
				case 45: //insert
					break;
				case 46: //delete
					break;
			}
		});

		/*
		oGrid.on( 'key', function (e, datatable, key, cell, originalEvent) {
			console.log( 'Tecla: ' + key );
		});
		*/

	};

	/**/

	this.Native = function() {
		return oGrid;
	};

	/**/

	this.Len = function() {
		var table = $( '#' + this.cId ).DataTable();
		return table.data().length;
	};

	/**/

	this.SetData = function( aData ) {
		var oTable = $( '#' + this.cId ).DataTable();

		if ( typeof( aData ) === 'undefined' )
		{
			aData = [];
		}

		oTable.clear();
		oTable.rows.add( aData );
		oTable.draw();
	};

	/**/

	this.GetData = function() {
		var table = $( '#' + this.cId ).DataTable();
		return table.data().toArray();
	};

	/**/

	this.GetItem = function() {
		return oRow;
	};

	/**/

	this.GetChanges = function() {
		
	};

	/**/

	this.InsertRow = function( aRow ) {
		var table = $( '#' + this.cId ).DataTable();

		if( isArray( aRow ) ) {
			table.row.add( aRow ).draw();
			return true;
		}
		return false;
	};

	/**/

	this.EditSelectRow = function() {
		var table = $( '#' + this.cId ).DataTable();

		if( this.Len() > 0 ) {
			if( table.rows( '.selected' ).any() ) {
				return this.GetItem();
			}
		}
		return null;
	};

	/**/

	this.DeleteSelectRow = function() {
		var table = $( '#' + this.cId ).DataTable();

		if( this.Len() > 0 ) {
			if( table.rows( '.selected' ).any() ) {
				table.row( '.selected' ).remove().draw( false );
				return true;
			}
		}
		return false;
	};

	/**/

	this.Refresh = function() {
		
	};

	/**/

	this.GetColValue = function( nCol ) {
		var table = $( '#' + this.cId ).DataTable();

		if( this.Len() > 0 ) {
			if( nCol >= 0 && nCol <= oRow.length ) {
				return oRow[ nCol ];
			}
		}
	};

	/**/

	this.SetFocus = function() {
		var table = $( '#' + this.cId ).DataTable();

		if( this.Len() > 0 ) {
			table.cell( ':eq(0)' ).focus();
			table.row(':eq(0)', { page: 'current' }).select();
		}
	};

	/**/

	this.Recno = function() {
		return nRecno;
	};

	/**/

	this.Version = function() {
		MsgNotify( 'TDataTable2, version 1.0a', 'success', true );
	};

	/**/

	this.Init();
}

/*-----------------------------------------------------------------*/

function isArray( o ) {
	return Object.prototype.toString.call( o ) === '[object Array]'; 
}

/*-----------------------------------------------------------------*/